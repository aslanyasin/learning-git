# git Öğreniyoruz

Git, dağıtık versiyon takip sistemidir. Bu repository üzerinde biz de bunun çalışmasını ve nasıl kullanabileceğimizi öğreniyoruz.

* Ders 1 - [DVCS Mantığı ve Git.](https://www.youtube.com/watch?v=lxsff7o0vjU&list=PL2jZMx6EhNFORQJU2-dyhy1KmuggAoudB)
* Ders 2 - [İlk depomuzu oluşturuyoruz.](https://www.youtube.com/watch?v=axog9HT0xL8&index=2&list=PL2jZMx6EhNFORQJU2-dyhy1KmuggAoudB)
* Ders 3 - [Dallar: Sapla Samanı Ayırma.](https://www.youtube.com/watch?v=j6baJi_eRwo&index=3&list=PL2jZMx6EhNFORQJU2-dyhy1KmuggAoudB)
